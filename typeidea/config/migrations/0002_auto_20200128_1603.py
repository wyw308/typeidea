# -*- coding: utf-8 -*-
# Generated by Django 1.11.27 on 2020-01-28 08:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('config', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='link',
            name='href',
            field=models.URLField(verbose_name='链接'),
        ),
    ]
